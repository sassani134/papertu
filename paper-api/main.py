from fastapi import FastAPI, status
from database import engine, Base, Player
from pydantic import BaseModel


# Create a PlayerRequest Base Model
class PlayerRequest(BaseModel):
    name: str
    lvl: int
    exp: int
    hp: int
    mp: int
    atk: int
    defn: int


# Create the database
Base.metadata.create_all(engine)

# Initialize app
app = FastAPI()


@app.get("/")
async def root():
    return {"message": "Hello World"}


# Read All
@app.get("/players")
async def get_players():
    return {"message": "Players"}


# Read One
@app.get("/players/{player_id}")
async def get_player(player_id: int):
    return {"message": f"Player {player_id}"}


# Update
@app.put("/players/{player_id}")
async def update_player(player_id: int):
    return {"message": f"Player {player_id} updated"}


# Delete
@app.delete("/players/{player_id}")
async def delete_player(player_id: int):
    return {"message": f"Player {player_id} deleted"}


# Create
@app.post("/players", status_code=201)  # status_code=status.HTTP_201_CREATED)
async def create_player(player: PlayerRequest):
    # Create a new database session
    session = Session(bind=engine, expire_on_commit=False)

    # Create a new player instance
    playerdbb = Player(  name=player.name, lvl=player.lvl, exp=player.exp, hp=player.hp, mp=player.mp, atk=player.atk, defn=player.defn )

    # Add it to the session and commit it to the database
    session.add(playerdbb)
    session.commit()
    id = playerdbb.id

    # Close the session
    session.close()

    # Return the id of the new player
    return f"Player {id} created"

