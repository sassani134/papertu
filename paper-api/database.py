from sqlalchemy import create_engine, Column, Integer, String
from sqlalchemy.ext.declarative import declarative_base


# Create a sqlite engine instance
engine = create_engine("sqlite:///players.db")

# Create a declarative base instance
Base = declarative_base()


# Create a Player class
class Player(Base):
    __tablename__ = "players"
    id = Column(Integer, primary_key=True)
    name = Column(String(256))
    lvl = Column(Integer)
    exp = Column(Integer)
    maxHp = Column(Integer)
    maxMp = Column(Integer)
    atk = Column(Integer)
    defn = Column(Integer)
    atkSpe = Column(Integer)
    defSpe = Column(Integer)
    img = Column(String)